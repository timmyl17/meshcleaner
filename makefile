CPP=$(wildcard src/*.cpp)
OBJ=$(addprefix obj/,$(notdir $(CPP:.cpp=.o)))
INC=-Iinclude -Iexternal/libigl/include -Iexternal/tetgen -I/usr/include/eigen3
TETDIR=external/tetgen/build
LIB=-lCGAL -lCGAL_Core -lgmp -lmpfr -L$(TETDIR) -ltet -lboost_thread -lboost_system

CFLAGS=-std=c++14 -O3 -march=native -frounding-math -fsignaling-nans

meshclean: obj $(OBJ) $(TETDIR)/libtet.a
	g++ -fopenmp $(CFLAGS) -o meshclean $(OBJ) $(LIB)

$(TETDIR)/libtet.a:
	cd external/tetgen
	mkdir build
	cd build
	cmake ..
	make
	cd ../../../

obj:
	mkdir -p obj

obj/%.o: src/%.cpp
	g++ -fopenmp $(CFLAGS) -c $< -o $@ $(INC)

clean:
	rm -f $(OBJ)
	rm -f meshclean
