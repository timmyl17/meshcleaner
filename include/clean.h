#ifndef _CLEAN_H
#define _CLEAN_H

// Taken from: https://raw.githubusercontent.com/libigl/libigl-examples/master/skeleton-poser/clean.h

#include <Eigen/Core>
// Given a mesh (V,F), fix self-intersections and open boundaries
//
// Inputs:
//   V  #V by 3 list of mesh vertex positions
//   F  #F by 3 list of mesh face indices into V
// Outputs:
//   CV  #CV by 3 list of mesh vertex positions
//   CF  #CF by 3 list of mesh face indices into CV
//   IM  >=#CV list of indices into V, so that (CV,IM(F)) produces the same
//     mesh as (V,F)
// Returns true iff success.
//
bool clean(const Eigen::MatrixXd & V,
           const Eigen::MatrixXi & F,
           Eigen::MatrixXd & CV,
           Eigen::MatrixXi & CF,
           Eigen::VectorXi & IM);

bool cleanMarchingCubes(const Eigen::MatrixXd & V,
                        const Eigen::MatrixXi & F,
                        Eigen::MatrixXd & CV,
                        Eigen::MatrixXi & CF,
                        Eigen::VectorXi & IM);

bool cleanMarchingCubesInterpolate(const Eigen::MatrixXd & V1,
                                   const Eigen::MatrixXi & F1,
                                   const Eigen::MatrixXd & V2,
                                   const Eigen::MatrixXi & F2,
                                   double t,
                                   Eigen::MatrixXd & CV,
                                   Eigen::MatrixXi & CF,
                                   const double h = .001);

std::pair<Eigen::MatrixXd, const Eigen::RowVector3i>
createGrid(const Eigen::MatrixXd & V1,
           const Eigen::MatrixXi & F1,
           const Eigen::MatrixXd & V2,
           const Eigen::MatrixXi & F2,
           const double h = .001);

Eigen::VectorXd
computeSDF(const Eigen::MatrixXd &V,
           const Eigen::MatrixXi &F,
           const Eigen::MatrixXd &GV);

bool runMarchingCubes(const Eigen::MatrixXd &GV,
                      const Eigen::RowVector3i &res,
                      const Eigen::VectorXd &S,
                      Eigen::MatrixXd & CV,
                      Eigen::MatrixXi & CF);

#endif // _CLEAN_H

