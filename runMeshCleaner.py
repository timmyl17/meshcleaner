#! /usr/bin/python

import os

inDir = '/home/tlangloi/gdrive/Research/fluid_sounds/damBreak/meshesNoRigidRefine/'
#outDir = '/mnt/Data/run_data/damBreak/tmpMeshes/'
outDir = '/home/tlangloi/workspace/research/wavesolver/runs/damBreak/run-debug/tmpMeshesDebug/'

tmpPrefix = 'interpTmpPrefixLaLa'

inFiles = sorted([f for f in os.listdir(inDir) if f.startswith('gmsh')])

interpNum = 8

#print(inFiles)

for f1, f2 in zip(inFiles[0:-1], inFiles[1:]):
    t1 = float(f1.split('-')[1])
    t2 = float(f2.split('-')[1])

    if t1 < .77 or t1 > .885:
        continue

    dt = (t2 - t1) / interpNum

    outFiles = [outDir + 'tmpMesh-%010.6f.obj' % (t1 + i * dt) for i in range(1, interpNum)]

    # If all the files already exist then nothing to do
    if all([os.path.isfile(f) for f in outFiles]):
        continue

    outPrefix = outDir + tmpPrefix

    cmd = './meshclean %s %s %s %i' % (inDir + f1, outPrefix, inDir + f2, interpNum)

    print(cmd)

    os.system(cmd)

    # Rename files
    tmpFiles = sorted([f for f in os.listdir(outDir) if f.startswith(tmpPrefix)])

    for inF, outF in zip(tmpFiles, outFiles):
        os.rename(outDir + inF, outF)

    break



    #for i in range(1, interpNum):
    #    t = t1 + i * dt
    #    #print('%010.6f' % t)

    #    tAmt = (t - t1) / (t2 - t1)
    #    outFile = outDir + 'tmpMesh-%010.6f.obj' % t

    #    cmd = './meshclean %s %s %s %f' % (inDir + f1, outFile, inDir + f2, tAmt)

    #    print(cmd)

    #    os.system(cmd)

