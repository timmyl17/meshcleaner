// Taken from: https://raw.githubusercontent.com/libigl/libigl-examples/master/skeleton-poser/clean.cpp

#include "clean.h"
#include <igl/barycenter.h>
#include <igl/boundary_facets.h>
#include <igl/copyleft/tetgen/tetrahedralize.h>
#include <igl/copyleft/tetgen/cdt.h>
#include <igl/winding_number.h>
#include <igl/unique_simplices.h>
#include <igl/remove_unreferenced.h>
#include <igl/writeOBJ.h>
#include <igl/writeMESH.h>
#include <igl/copyleft/cgal/remesh_self_intersections.h>
#include <igl/REDRUM.h>
#include <igl/circulation.h>
#include <igl/collapse_edge.h>
#include <igl/edge_flaps.h>
#include <igl/shortest_edge_and_midpoint.h>
#include <igl/remove_duplicates.h>
#include <igl/copyleft/marching_cubes.h>
#include <igl/signed_distance.h>
#include <iostream>

bool clean(const Eigen::MatrixXd & V,
           const Eigen::MatrixXi & F,
           Eigen::MatrixXd & CV,
           Eigen::MatrixXi & CF,
           Eigen::VectorXi & IM)
{
    using namespace igl;
    using namespace igl::copyleft::tetgen;
    using namespace igl::copyleft::cgal;
    using namespace Eigen;
    using namespace std;
    //writeOBJ("VF.obj",V,F);

    // Remove duplicate verts
    // Using this with a high tolerance, to simplify the mesh a bit
    MatrixXd NV;
    MatrixXi NF;
    {
        VectorXi I;
        remove_duplicates(V, F, NV, NF, I, 3e-3);
    }

    // Remesh self intersections
    {
        MatrixXi _1;
        VectorXi _2;
        cout<<"clean: remesh_self_intersections"<<endl;
        remesh_self_intersections(NV,NF,{false,false,false},CV,CF,_1,_2,IM);
        //for_each(CF.data(),CF.data()+CF.size(),[&IM](int & a){a=IM(a);});
        cout<<"clean: remove_unreferenced"<<endl;
        {
            MatrixXi oldCF = CF;
            unique_simplices(oldCF,CF);
        }
        MatrixXd oldCV = CV;
        MatrixXi oldCF = CF;
        VectorXi nIM;
        remove_unreferenced(oldCV,oldCF,CV,CF,nIM);
        // reindex nIM through IM
        //for_each(IM.data(),IM.data()+IM.size(),[&nIM](int & a){a=a>=0?nIM(a):a;});
    }

    MatrixXd TV;
    MatrixXi TT;
    {
        MatrixXi _1;
        // c  convex hull
        // Y  no boundary steiners
        // p  polygon input
        // T1e-16  sometimes helps tetgen
        cout<<"clean: tetrahedralize"<<endl;
        //writeOBJ("CVCF.obj",CV,CF);
        CDTParam params;
        //params.flags = "CYT1e-16";
        params.use_bounding_box = true;
        if(cdt(CV,CF,params,TV,TT,_1) != 0)
        {
            cout<<REDRUM("CDT failed.")<<endl;
            return false;
        }
        //writeMESH("TVTT.mesh",TV,TT,MatrixXi());
    }

    // Keep only interior tets and take boundary
    {
        MatrixXd BC;
        barycenter(TV,TT,BC);
        VectorXd W;
        cout<<"clean: winding_number"<<endl;
        winding_number(V,F,BC,W);
        W = W.array().abs();
        const double thresh = 0.95;
        const int count = (W.array()>thresh).cast<int>().sum();
        MatrixXi CT(count,TT.cols());
        int c = 0;
        for(int t = 0;t<TT.rows();t++)
        {
            if(W(t)>thresh)
            {
                CT.row(c++) = TT.row(t);
            }
        }
        assert(c==count);
        boundary_facets(CT,CF);
        MatrixXd oldTV = TV;
        MatrixXi oldCF = CF;
        VectorXi nIM;
        remove_unreferenced(oldTV,oldCF,CV,CF,nIM);

        //writeMESH("CVCTCF.mesh",TV,CT,CF);
    }
    return true;
}

bool cleanMarchingCubes(const Eigen::MatrixXd & V,
                        const Eigen::MatrixXi & F,
                        Eigen::MatrixXd & CV,
                        Eigen::MatrixXi & CF,
                        Eigen::VectorXi & IM)
{
    using namespace igl;
    using namespace igl::copyleft::tetgen;
    using namespace igl::copyleft::cgal;
    using namespace Eigen;
    using namespace std;

    const double h = 0.001;
    RowVector3d slop(.002, .002, .002);

    // number of vertices on the largest side
    const RowVector3d Vmin = V.colwise().minCoeff() - slop;
    const RowVector3d Vmax = V.colwise().maxCoeff() + slop;
    const int s = std::ceil((Vmax-Vmin).maxCoeff()/h);
    const RowVector3i res = (s*((Vmax-Vmin)/(Vmax-Vmin).maxCoeff())).cast<int>();

    // create grid
    cout<<"Creating grid..."<<endl;
    MatrixXd GV(res(0)*res(1)*res(2),3);
    for(int zi = 0;zi<res(2);zi++)
    {
        const auto lerp = [&](const int di, const int d)->double
            {return Vmin(d)+(double)di/(double)(res(d)-1)*(Vmax(d)-Vmin(d));};
        const double z = lerp(zi,2);
        for(int yi = 0;yi<res(1);yi++)
        {
            const double y = lerp(yi,1);
            for(int xi = 0;xi<res(0);xi++)
            {
                const double x = lerp(xi,0);
                GV.row(xi+res(0)*(yi + res(1)*zi)) = RowVector3d(x,y,z);
            }
        }
    }

    // compute values
    cout<<"Computing distances..."<<endl;
    VectorXd S,B;
    {
        VectorXi I;
        MatrixXd C,N;
        signed_distance(GV,V,F,SIGNED_DISTANCE_TYPE_WINDING_NUMBER,S,I,C,N);
    }
    cout<<"Marching cubes..."<<endl;
    igl::copyleft::marching_cubes(S,GV,res(0),res(1),res(2),CV,CF);

    return true;
}

std::pair<Eigen::MatrixXd, const Eigen::RowVector3i>
createGrid(const Eigen::MatrixXd & V1,
           const Eigen::MatrixXi & F1,
           const Eigen::MatrixXd & V2,
           const Eigen::MatrixXi & F2,
           const double h)
{
    using namespace igl;
    using namespace igl::copyleft::tetgen;
    using namespace igl::copyleft::cgal;
    using namespace Eigen;
    using namespace std;

    //const double h = 0.001;

    RowVector3d slop(2*h, 2*h, 2*h);

    // number of vertices on the largest side
    const RowVector3d Vmin = V1.colwise().minCoeff().cwiseMin(V2.colwise().minCoeff()) - slop;
    const RowVector3d Vmax = V1.colwise().maxCoeff().cwiseMax(V2.colwise().maxCoeff()) + slop;
    const int s = std::ceil((Vmax-Vmin).maxCoeff()/h);
    const RowVector3i res = (s*((Vmax-Vmin)/(Vmax-Vmin).maxCoeff())).cast<int>();

    // create grid
    cout<<"Creating grid..."<<endl;
    MatrixXd GV(res(0)*res(1)*res(2),3);
    for(int zi = 0;zi<res(2);zi++)
    {
        const auto lerp = [&](const int di, const int d)->double
            {return Vmin(d)+(double)di/(double)(res(d)-1)*(Vmax(d)-Vmin(d));};
        const double z = lerp(zi,2);
        for(int yi = 0;yi<res(1);yi++)
        {
            const double y = lerp(yi,1);
            for(int xi = 0;xi<res(0);xi++)
            {
                const double x = lerp(xi,0);
                GV.row(xi+res(0)*(yi + res(1)*zi)) = RowVector3d(x,y,z);
            }
        }
    }

    return std::make_pair(GV, res);
}

Eigen::VectorXd
computeSDF(const Eigen::MatrixXd &V,
           const Eigen::MatrixXi &F,
           const Eigen::MatrixXd &GV)
{
    using namespace igl;
    using namespace Eigen;
    using namespace std;

    // compute values
    cout<<"Computing distances..."<<endl;
    VectorXd S;
    {
        VectorXi I;
        MatrixXd C,N;
        signed_distance(GV,V,F,SIGNED_DISTANCE_TYPE_WINDING_NUMBER,S,I,C,N);
    }

    return S;
}

bool runMarchingCubes(const Eigen::MatrixXd &GV,
                      const Eigen::RowVector3i &res,
                      const Eigen::VectorXd &S,
                      Eigen::MatrixXd & CV,
                      Eigen::MatrixXi & CF)
{
    using namespace igl;
    using namespace Eigen;
    using namespace std;

    cout<<"Marching cubes..."<<endl;
    igl::copyleft::marching_cubes(S,GV,res(0),res(1),res(2),CV,CF);
}

// Interpolate between two meshes
// t == 0 = mesh1
// t == 1 = mesh2
bool cleanMarchingCubesInterpolate(const Eigen::MatrixXd & V1,
                                   const Eigen::MatrixXi & F1,
                                   const Eigen::MatrixXd & V2,
                                   const Eigen::MatrixXi & F2,
                                   double t,
                                   Eigen::MatrixXd & CV,
                                   Eigen::MatrixXi & CF,
                                   const double h)
{
    using namespace igl;
    using namespace igl::copyleft::tetgen;
    using namespace igl::copyleft::cgal;
    using namespace Eigen;
    using namespace std;

    assert(t >= 0 && t <= 1);

    //const double h = 0.001;

    RowVector3d slop(2*h, 2*h, 2*h);

    // number of vertices on the largest side
    const RowVector3d Vmin = V1.colwise().minCoeff().cwiseMin(V2.colwise().minCoeff()) - slop;
    const RowVector3d Vmax = V1.colwise().maxCoeff().cwiseMax(V2.colwise().maxCoeff()) + slop;
    const int s = std::ceil((Vmax-Vmin).maxCoeff()/h);
    const RowVector3i res = (s*((Vmax-Vmin)/(Vmax-Vmin).maxCoeff())).cast<int>();

    // create grid
    cout<<"Creating grid..."<<endl;
    MatrixXd GV(res(0)*res(1)*res(2),3);
    for(int zi = 0;zi<res(2);zi++)
    {
        const auto lerp = [&](const int di, const int d)->double
            {return Vmin(d)+(double)di/(double)(res(d)-1)*(Vmax(d)-Vmin(d));};
        const double z = lerp(zi,2);
        for(int yi = 0;yi<res(1);yi++)
        {
            const double y = lerp(yi,1);
            for(int xi = 0;xi<res(0);xi++)
            {
                const double x = lerp(xi,0);
                GV.row(xi+res(0)*(yi + res(1)*zi)) = RowVector3d(x,y,z);
            }
        }
    }

    // compute values
    cout<<"Computing distances..."<<endl;
    VectorXd S;
    {
        VectorXd S1, S2;
        VectorXi I;
        MatrixXd C,N;
        signed_distance(GV,V1,F1,SIGNED_DISTANCE_TYPE_WINDING_NUMBER,S1,I,C,N);

        signed_distance(GV,V2,F2,SIGNED_DISTANCE_TYPE_WINDING_NUMBER,S2,I,C,N);

        S = (1. - t) * S1 + t * S2;
    }

    cout<<"Marching cubes..."<<endl;
    igl::copyleft::marching_cubes(S,GV,res(0),res(1),res(2),CV,CF);

    return true;
}
