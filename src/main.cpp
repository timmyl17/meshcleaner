
#include <set>
#include <igl/read_triangle_mesh.h>
#include <igl/write_triangle_mesh.h>
#include <iostream>
#include <clean.h>
#include <Eigen/Dense>
#include <Mesh.hpp>

void
extractSurfaceToIGL(const Mesh *m,
                    Eigen::MatrixXd &V,
                    Eigen::MatrixXi &F)
{
    std::map<int, int> vertsToKeep;
    for (const auto& t : m->m_surfTris)
    {
        if (m->m_triType.at(t) == Mesh::FLUID_AIR || m->m_triType.at(t) == Mesh::SOLID)
        {
            vertsToKeep[m->m_triangles[t](0)] = -1;
            vertsToKeep[m->m_triangles[t](1)] = -1;
            vertsToKeep[m->m_triangles[t](2)] = -1;
        }
    }

    V.resize(vertsToKeep.size(), 3);

    int counter = 1;
    for (auto& ve : vertsToKeep)
    {
        const auto& v = m->m_vertices.at(ve.first);

        V.row(counter-1) = m->m_vertices.at(ve.first).transpose();
        ve.second = counter++;
    }

    int nSurfTris = 0;
    std::set<int> badTris;
    for (int i = 0; i < m->m_surfTris.size(); ++i)
    {
        const auto& t = m->m_surfTris[i];
        if (m->m_triType.at(t) == Mesh::FLUID_AIR || m->m_triType.at(t) == Mesh::SOLID)
        {
            bool degenerate = m->triangleArea(i) < 1e-7;
            if (degenerate)
            {
                badTris.insert(i);
            }
            else
            {
                ++nSurfTris;
            }
        }
    }

    F.resize(nSurfTris, 3);
    counter = 0;
    for (int i = 0; i < m->m_surfTris.size(); ++i)
    {
        const auto& t = m->m_surfTris[i];

        if (m->m_triType.at(t) == Mesh::FLUID_AIR || m->m_triType.at(t) == Mesh::SOLID)
        {
            bool degenerate = badTris.count(i) > 0;

            if (!degenerate)
            {
                F.row(counter++) << vertsToKeep[m->m_triangles[t](0)]-1,
                                    vertsToKeep[m->m_triangles[t](1)]-1,
                                    vertsToKeep[m->m_triangles[t](2)]-1;
            }
        }
    }
}

int main(int argc, char * argv[])
{
    // Load in libigl's (V,F) format
    Eigen::MatrixXd V,W;
    Eigen::MatrixXi F,G;
    if(argc <= 2)
    {
        std::cout<<R"(Usage:
    ./meshclean [input](.obj|.ply|.stl|.off|.msh) [output](.obj|.ply|.stl|.off) [optional input2] [n]
)";

        std::cout << "If using optional inputs, output is just a base path" << std::endl;
        return EXIT_FAILURE;
    }

    // Read gmsh if requested
    int len = strlen(argv[1]);

    assert(len > 3);

    if (strcmp(argv[1] + len - 3, "msh") == 0)
    {
    	Mesh m;
    	m.loadGmsh(argv[1]);
    	extractSurfaceToIGL(&m, V, F);
    }
    else
    {
        igl::read_triangle_mesh(argv[1],V,F);
    }

    Eigen::VectorXi IM;
    //clean(V,F,W,G,IM);
    if (argc == 3)
    {
        cleanMarchingCubes(V,F,W,G,IM);

        // Write to OBJ
        igl::write_triangle_mesh(argv[2],W,G);
    }
    else if (argc == 5)
    {
        Eigen::MatrixXd V1;
        Eigen::MatrixXi F1;
        len = strlen(argv[3]);

        assert(len > 3);

        if (strcmp(argv[3] + len - 3, "msh") == 0)
        {
            Mesh m;
            m.loadGmsh(argv[3]);
            extractSurfaceToIGL(&m, V1, F1);
        }
        else
        {
            igl::read_triangle_mesh(argv[3],V1,F1);
        }

        int n = std::stoi(argv[4]);

        if (n < 1|| n % 2)
        {
            std::cout << "Bad n value, must > 1 and even" << std::endl;
            return 1;
        }

        auto grid = createGrid(V, F, V1, F1);
        auto SDF1 = computeSDF(V, F, grid.first);
        auto SDF2 = computeSDF(V1, F1, grid.first);

        for (int i = 1; i < n; ++i)
        {
            double t = (i) / static_cast<double>(n);

            auto curS = (1. - t) * SDF1 + t * SDF2;
            runMarchingCubes(grid.first, grid.second, curS, W, G);

            std::ostringstream os;
            os << argv[2] << "_" << i << "_.obj";

            // Write to OBJ
            igl::write_triangle_mesh(os.str(),W,G);
        }
    }
    else
    {
        std::cout << "bad number of arguments" << std::endl;
        return 1;
    }
}
